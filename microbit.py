# programa para mostrar lo que se pasa a puerto serie
from microbit import *

def main():
    uart.init(baudrate=9600)
    while True:
        sleep(500)
        msg = uart.read()
        if (msg is not None):
            display.show(str(msg, 'UTF-8'))
            msg = "Se ha recibido: " + str(msg)[2:-1]
            print(str(msg, 'UTF-8'))

        else:
            display.clear()
if __name__ == "__main__":
    main()
